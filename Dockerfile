FROM node:16-alpine

WORKDIR /usr/src/app

ARG NODE_ENV
ENV NODE_ENV $NODE_ENV

COPY . /usr/src/app

COPY package*.json /usr/src/app/
RUN npm install
RUN npm install --save @sentry/node@7.14.0 @sentry/tracing@7.14.0

ENV PORT 5000
EXPOSE $PORT
CMD [ "npm", "start" ]
